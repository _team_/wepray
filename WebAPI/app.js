
/**
 * Module dependencies.
 */

var express = require('express');
//        , routes = require('./routes')
//        , user = require('./routes/user')
//        , examinations = require('./routes/examinations')
//        , feedback = require('./routes/feedback')
//        , category = require('./routes/category')
//        , examSetup = require('./routes/examsetup')
//        , http = require('http')
//        , path = require('path')
//        , nodemailer = require("nodemailer");

var app = express();

app.configure(function() {
    app.set('port', process.env.PORT || 3000);
    app.set('views', __dirname + '/views');
    app.set('view engine', 'ejs');
    app.use(express.favicon());
    app.use(express.logger('dev'));
    app.use(express.bodyParser());
    app.use(express.methodOverride());
    app.use(express.cookieParser('your secret here'));
//    app.use(express.session());
    app.use(app.router);
    // New call to compress content
    app.use(express.compress());
    app.use(require('stylus').middleware(__dirname + '/public'));
    app.use(express.static(path.join(__dirname, 'public')));
    app.use(function(req, res, next) {
        res.render('404', {status: 404, url: req.url});
    });
});

app.configure('development', function() {
    app.use(express.errorHandler());
});

app.get('/', routes.index);


//app.get('/:category', category.examlist);
//app.get('/exam/:category/:id', examinations.list);
//app.get('/:tenantname/examsetup', examSetup.exams);
//app.get('/users', user.list);
//app.post("/feedback", feedback.sendFeedback);

http.createServer(app).listen(app.get('port'), function() {
    console.log("Express server listening on port " + app.get('port'));
});

//Catch uncaught error, otherwise server crash => server down
process.on('uncaughtException', function(error) {
    console.log("Uncaught exception" + error.stack);
    var smtpTransport = nodemailer.createTransport("SMTP", {
        service: "Gmail",
        auth: {
            user: "rezwan.cse.02@gmail.com",
            pass: "buet1234"
        }
    });

    var mailOptions = {
        from: "Test Now <rezwan.cse.02@gmail.com>", // sender address
        to: "tamim@csebuet.org", // list of receivers
        subject: "Support", // Subject line
        text: error.stack, // plaintext body
    }

    smtpTransport.sendMail(mailOptions, function(error, response) {
        if (error) {
            console.log(error);
        } else {
            console.log("Message sent: " + response.message);
        }

        // if you want to reuse this transport object, comment following line
        smtpTransport.close(); // shut down the connection pool, no more messages
    });
});